import { rgb24 } from 'https://deno.land/std@0.68.0/fmt/colors.ts';

// since it gets stringified, allow most formats
interface Color {
  r: bigint | number | string,
  g: bigint | number | string,
  b: bigint | number | string,
}

export async function getColorData(col: Color) {
  // fetch color data from API
  const rgb = `rgb(${col.r}, ${col.g}, ${col.b})`;
  const res = await fetch(`https://www.thecolorapi.com/id?rgb=${rgb}`);
  const { name, hex } = await res.json();

  // create colored text using color hex
  const colStr = rgb24(hex.value, parseInt(hex.clean, 16));

  // create colored text using color hex
  const namedRgbNum = parseInt(name.closest_named_hex.slice(1), 16);
  const nameColStr = rgb24(name.value, namedRgbNum);

  return {
    colStr,
    nameColStr
  }
}