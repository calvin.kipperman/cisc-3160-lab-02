import { decode } from 'https://cdn.skypack.dev/fast-png@^5.0.2';
import { getColorData } from './utils.ts';

const imgUrl = Deno.args[0];

// ensure we're passed a file path by throwing an error if not
if (!imgUrl) {
  throw new Error('please provide an img path argument')
}
if (!imgUrl.endsWith('.png')) {
  throw new Error('please provide a png file')
}

// we get a Uint8Array and then decode it using the lib. We pass undefined so TS doesn't yell at us
const file = await Deno.readFile(imgUrl);
const { width, height, data } = decode(file, undefined);

// log the width and height. Size can easily be gotten from the byte array
console.log(`width: ${width}px | height: ${height}px`);
console.log(`size: ${~~(file.length / 1000)}kb`);

// we use BigInt in case there are many many pixels
const avgCol = {
  r: 0n,
  g: 0n,
  b: 0n,
};

// storing common color in a tuple
let commonCol: [string, number] = ['0,0,0', 0];

// massive HashMap of all colors to track the most commonly used color
const allColors = new Map();

// iterate by 4 bytes since colors are in 4 byte pairs
for (let i = 0, pixelNum = 0; i < data.length; i += 4, pixelNum++) {
  const r = data[i];
  const g = data[i + 1];
  const b = data[i + 2];
  const a = data[i + 3];
  // if we have an alpha of zero, skip it. This is invisible
  if (a === 0) continue;

  // make color into a string. Could probably do hex, but this is easier for now
  const str = `${r},${g},${b}`;
  const times = allColors.get(str) || 0;
  allColors.set(str, times + 1);
  if (commonCol[1] <= times) {
    commonCol = [str, times + 1];
  }
  
  // add color to average. We will divide later
  avgCol.r += BigInt(r);
  avgCol.g += BigInt(g);
  avgCol.b += BigInt(b);
}

// divide by length to get average.
const len = BigInt(data.length / 4)
avgCol.r /= len;
avgCol.g /= len;
avgCol.b /= len;

// print average color
const colData1 = await getColorData(avgCol);
console.log(`average color: ${colData1.colStr}`);
console.log(`closest color name to average color: ${colData1.nameColStr}`);

// print most common color
const [r, g, b] = commonCol[0].split(',');
const colData2 = await getColorData({ r, g, b });
console.log(`most common color: ${colData2.colStr}`);
console.log(`closest color name to common color: ${colData2.nameColStr}`);