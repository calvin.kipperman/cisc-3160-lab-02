import requests
import sys, math, io

from utils import get_lines
from cli import get_user_args
from filter import filter

def append_unicode(line):
  hex = line.split(';')[0]
  codepoint = int(line.split(';')[0], 16)
  
  char = '[ERROR]'
  try:
    char = chr(codepoint)
  except:
    pass

  return f"{char};{line}" 

if __name__ == '__main__':
  # allowing filtering options, as well as column sort
  args = get_user_args()

  # get UnicodeData v13
  response = requests.get("https://www.unicode.org/Public/13.0.0/ucd/UnicodeData.txt")
  unicode = response.text

  # retrieve all lines from file, filter it, and sort it
  lines_iter = get_lines(unicode)
  filtered_lines_iter = filter(args, lines_iter)
  lines = list(filtered_lines_iter)

  column = args["column"]
  if column != 0:
    sort_func = lambda line: line.split(';')[column]
    lines.sort(key=sort_func)

  if args["append_unicode"]:
    lines = map(append_unicode, lines)

  print("Done! Check output.txt for the sorted and filtered result!")
  
  # write sorted list to file.
  with io.open("output.txt", "w", encoding="utf-8") as file:
    file.write('\n'.join(lines))
