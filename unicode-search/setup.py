from setuptools import setup

setup(name='unicode-search',
      version='0.1',
      description='lookup unicode from the command line',
      license='MIT',
      install_requires=[
        'requests',
      ])