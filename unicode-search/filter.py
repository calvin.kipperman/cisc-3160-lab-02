
# yield lines only including certain text (case insensitive)
def filter_text(filter_text, lines_iter):
  text = filter_text.upper()

  for line in lines_iter:
    if text in line.split(';')[1]:
      yield line

# yield lines only between 2 codepoints
def filter_range(codepoint_range, lines_iter):
  for line in lines_iter:
    hex = line.split(';')[0]
    num = int(hex, 16)
    if num >= codepoint_range[0] and num <= codepoint_range[1]:
      yield line

# filter by range, name, and then only under limit
def filter(filter_dict, lines_iter):
  iter = filter_text(filter_dict["filter_text"], lines_iter)
  iter = filter_range(filter_dict["codepoint_range"], iter)

  limit = filter_dict["limit"]

  for i, line in enumerate(iter):
    if i + 1 > limit:
      break

    yield line