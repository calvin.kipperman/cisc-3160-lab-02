# a bit of an efficient way to parse through the unicode file, using a generator
def get_lines(file):
  starting_index = 0
  ignore = False

  for i, char in enumerate(file):
    # if this is a comment, ignore the rest of this line
    if char == '#':
      ignore = True
      
      # if we aren't at the start of the line, check for non-whitespace text before the #
      if i - starting_index > 1:
        curr_line = file[starting_index:i].strip()
        if len(curr_line) > 0:
          yield curr_line

      continue

    # if this is a non-special case char, skip and keep moving
    if char != '\n':
      continue

    # if newline and we were told to ignore, ignore it
    if ignore:
      ignore = False
      continue

    # if at the end of a line and we don't ignore, yield the line and move the starting index  
    yield file[starting_index:i]
    starting_index = i + 1

def show_help(options):
  min_len = 0

  for option, desc in options:
    if len(option) > min_len:
      min_len = len(option)
  
  for option, desc in options:
    whitespace = " " * (min_len - len(option))
    print("\t" + option + whitespace + "\t= " + desc)
