import sys, math
from utils import show_help

# custom function to parse args since the default kinda sucks
def parse_args(args):
  name = ""
  vals = []

  for arg in args:
    if arg[0] != '-':
      vals.append(arg)
      continue

    if len(name) + len(vals) > 0:
      arg_str = ' '.join(vals)
      
      # yield the name and arg to user
      yield (name, arg_str)
      
      vals.clear()
    
    name = arg

  # after loop is done, if we have any args, yield it
  if len(name) + len(vals) > 0:
    yield (name, ' '.join(vals))

def get_user_args():
  # default values
  filter_text = ''
  codepoint_range = [0, math.inf]
  limit = math.inf
  column = 0
  append_unicode = False

  args = parse_args(sys.argv[1:])

  for name, arg in args:
    if name in ('', '-c', '--column'):
      column = int(arg)
    elif name in ('-t', '-ft', '--filter-text'):
      filter_text = arg
    elif name in ('-s', '-sc', '--starting-codepoint'):
      codepoint_range[0] = int(arg)
    elif name in ('-e', '-ec', '--ending-codepoint'):
      codepoint_range[1] = int(arg)
    elif name in ('-l', '--limit'):
      limit = int(arg)
    elif name in ('-a', '--append-unicode'):
      append_unicode = True if arg.lower() != 'false' else False
    else:
      # help user out and then exit
      print("python main.py <options>")

      show_help([
        ("<column>", "which column to sort by. Default=0"),
        ("-t <filter_text>", "text to ensure is included in unicode names. Case insensitive"),
        ("-s <starting_codepoint>", "only include unicode from this codepoint"),
        ("-e <ending_codepoint>", "only include unicode until this codepoint"),
        ("-l <limit>", "only include this many in output.txt"),
        ("-a", "add the unicode symbols to the output.txt"),
      ])
      sys.exit()
  
  return {
    "column": column,
    "filter_text": filter_text,
    "codepoint_range": codepoint_range,
    "limit": limit,
    "append_unicode": append_unicode,
  }