# Lab 02

## Calling API | Deno
png-data is a CLI program which returns some data about a PNG.
It gives the dimensions, size, average color, and most common color.

### Usage
Make sure [Deno](https://deno.land/) is installed and properly configured.
```bash
> deno run --allow-read --allow-net mod.ts C:\Path\to\my\picture.png
width: 1024px | height: 1024px
size: 218kb
average color: #ABABAB
closest color name to average color: Silver Chalice
most common color: #FFFFFF
closest color name to common color: White
```

`-A` can be used instead of `--allow-read --allow-net` althought it's usually recommended to be specific about program permissions.

## Math Operations | Rust
utf-8 encoder is a CLI program which returns the UTF-8 encoded byte array of a given string.
If passed a path of a valid file, it will use the text from the file as the byte array.
If passed a path as the second argument, it will create such a file and output the text to that file.

### Usage
Make sure [Rust](https://www.rust-lang.org/) and [Cargo](https://doc.rust-lang.org/cargo/getting-started/installation.html) are properly installed and properly configured.
```bash
> cargo build

> target\debug\utf8-encoder.exe "hello"
1101000, 1100101, 1101100, 1101100, 1101111

> target\debug\utf8-encoder.exe "hello" .\message.txt
successfully created a UTF-8 file!

> target\debug\utf8-encoder.exe .\message.txt
1101000, 1100101, 1101100, 1101100, 1101111
```

The general parameter order is `<text/path> <optional_output_path>`.
The bytes will be output to stdout if no output path is provided.

## CSV Sort | Python
unicode-search is a CLI program which allows you to filter through the entire unicode database.
It can filter by name, codepoint range, and max outputs. It can then also sort the lines by a column.
It outputs to `output.txt`. It will also print the characters to stdout if told to using `-p`.

### Usage
Make sure [python](https://www.python.org/) is properly installed and properly configured. This uses the `requests` module. It is the only third party module, so make sure it's installed.
```
> python main.py
Done! Check output.txt for the sorted and filtered result!

> python main.py --help
python main.py <options>
  <column>                = which column to sort by. Default=0
  -t <filter_text>        = text to ensure is included in unicode names. Case insensitive
  -s <starting_codepoint> = only include unicode from this codepoint
  -e <ending_codepoint>   = only include unicode until this codepoint
  -l <limit>              = only include this many in output.txt
  -a                      = add the unicode symbols to the output.txt
```