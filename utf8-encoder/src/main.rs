use std::{ 
    fs::{ File, read_to_string }, 
    io::prelude::*,
    path::Path,
};

mod lib;
use lib::encode_utf8;

// returns text file contents if it's a valid path. If not, the text itself
fn get_text_from_arg(arg: &String) -> String {
    let path = Path::new(arg);

    match read_to_string(path) {
        Ok(text) => text,
        Err(_) => arg.to_owned(),
    }
}

fn main() { 
    // should be a path to a file or some random text
    let arg1 = std::env::args().nth(1).expect("please provide text or a path to a text file");

    let text = get_text_from_arg(&arg1);

    let bytes = encode_utf8(&text);

    // write to file if second argument present
    match std::env::args().nth(2) {
        Some(arg) => {
            let path = Path::new(&arg);
            let mut file = File::create(path).expect("Could not create output file at that location");

            match file.write(&bytes) {
                Ok(_) => println!("successfully created a UTF-8 file!"),
                Err(e) => println!("{}", e),
            }
        },
        None => {
            let byte_str_vec = bytes.iter().map(|n| format!("{:b}", n)).collect::<Vec<String>>();
            let byte_str = byte_str_vec.join(", ");
            println!("{}", byte_str);
        }
    }
}
