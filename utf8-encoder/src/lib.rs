pub fn encode_utf8(text: &String) -> Vec<u8> {
	let mut vec: Vec<u8> = vec![];
	
	for char in text.chars() {
		let codepoint = char as u32;

		// perform no alteration if it can fit in 7 bits
		if codepoint < 128 {
			vec.push(codepoint as u8);
			continue;
		}
		
		// prefix last 5 bits with 110 and then the first 6 with 10
		if codepoint < 2048 {
			let num1 = 0b11000000 | (codepoint >> 6) as u8;
			let num2 = 0b10000000 | (codepoint & 0b111111) as u8;
			
			vec.push(num1);
			vec.push(num2);
			continue;
		}
		
		// prefix last 4 bits with 1110, middle 6 with 10, and first 6 with 10
		if codepoint < 65536 {
			let num1 = 0b11100000 | (codepoint >> 12) as u8;
			let num2 = 0b10000000 | ((codepoint >> 6) & 0b111111) as u8;
			let num3 = 0b10000000 | (codepoint & 0b111111) as u8;
			
			vec.push(num1);
			vec.push(num2);
			vec.push(num3);
			continue;
		}
		
		// prefix first 3 bits with 11110 and then the next 4 sets of 6 bits with 10
		let num1 = 0b11110000 | (codepoint >> 18) as u8;
		let num2 = 0b10000000 | ((codepoint >> 12) & 0b111111) as u8;
		let num3 = 0b10000000 | ((codepoint >> 6) & 0b111111) as u8;
		let num4 = 0b10000000 | (codepoint & 0b111111) as u8;
		
		vec.push(num1);
		vec.push(num2);
		vec.push(num3);
		vec.push(num4);
	}
	
	return vec;
}